from rest_framework.serializers import (
    ModelSerializer,
    SerializerMethodField
)
from school.models import School
from ..models import Student
from rest_framework import serializers


class StudentSchoolSerializer(ModelSerializer):
    class Meta:
        model = School
        fields = [
            'id',
            'name',
        ]


class StudentListSerializer(ModelSerializer):
    age = serializers.ReadOnlyField()

    class Meta:
        model = Student
        fields = [
            'id',
            'age',
            'first_name',
            'last_name',
            'date_of_birth'
        ]


class StudentRegisterSerializer(ModelSerializer):
    class Meta:
        model = Student
        fields = '__all__'


class StudentDetailSerializer(ModelSerializer):
    schools = StudentSchoolSerializer(read_only=True)
    is_adult = SerializerMethodField()

    class Meta:
        model = Student
        fields = '__all__'

    def get_is_adult(self, obj):
        if obj.age >= 18:
            return True
        return False


class StudentUpdateDeleteSerializer(ModelSerializer):
    class Meta:
        model = Student
        fields = '__all__'
