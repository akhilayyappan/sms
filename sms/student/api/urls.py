from django.urls import path
from .views import StudentListAPIView, StudentRegisterAPIView, StudentDetailAPIView, StudentUpdateAPIView, StudentDeleteAPIView

urlpatterns = [
    path('<int:pk>/list/', StudentListAPIView.as_view(), name='students-list'),
    path('register/', StudentRegisterAPIView.as_view(), name='register'),
    path('<int:pk>/detail/', StudentDetailAPIView.as_view(), name='students-detail'),
    path('<int:pk>/update/', StudentUpdateAPIView.as_view(), name='students-update'),
    path('<int:pk>/delete/', StudentDeleteAPIView.as_view(), name='students-delete'),

]
