from datetime import date
from rest_framework.pagination import LimitOffsetPagination
from rest_framework.filters import SearchFilter
from django.utils.timezone import now
from rest_framework.generics import (
    CreateAPIView,
    ListAPIView,
    UpdateAPIView,
    RetrieveAPIView,
    DestroyAPIView,
)

from ..models import Student

from rest_framework.permissions import (
    AllowAny,
    IsAuthenticated
)
from rest_framework.serializers import ValidationError

from .serializers import StudentListSerializer, StudentRegisterSerializer, StudentDetailSerializer, \
    StudentUpdateDeleteSerializer


class StudentListAPIView(ListAPIView):
    """
    API to get list of students in a school
    use age as query param to search based on the age.
    """
    serializer_class = StudentListSerializer
    permission_classes = [IsAuthenticated]
    lookup_url_kwarg = 'pk'
    filter_backends = [SearchFilter]
    search_fields = ['first_name', 'last_name']
    pagination_class = LimitOffsetPagination

    def get_queryset(self):

        school_id = self.kwargs.get(self.lookup_url_kwarg)
        age = self.request.query_params.get('age', None)
        if age:
            current = now().date()
            age = int(age)
            if not isinstance(age, int):
                raise ValidationError('Age should be number')
            min_age = age
            max_age = age + 1
            min_date = date(current.year - min_age, current.month, current.day)
            max_date = date(current.year - max_age, current.month, current.day)
            return Student.objects.filter(schools=school_id, date_of_birth__gt=max_date,
                                          date_of_birth__lte=min_date).order_by('first_name')
        return Student.objects.filter(schools=school_id).order_by('first_name')


class StudentRegisterAPIView(CreateAPIView):
    """
    API to register the students
    """
    serializer_class = StudentRegisterSerializer
    permission_classes = [AllowAny]
    queryset = Student.objects.all()


class StudentDetailAPIView(RetrieveAPIView):
    """
    API to get student details
    """
    serializer_class = StudentDetailSerializer
    permission_classes = [IsAuthenticated]
    lookup_field = 'pk'
    queryset = Student.objects.all()


class StudentUpdateAPIView(UpdateAPIView):
    """
    API to get update student object
    """
    serializer_class = StudentUpdateDeleteSerializer
    permission_classes = [IsAuthenticated]
    lookup_field = 'pk'
    queryset = Student.objects.all()


class StudentDeleteAPIView(DestroyAPIView):
    """
    API to delete student object
    """
    serializer_class = StudentUpdateDeleteSerializer
    permission_classes = [IsAuthenticated]
    lookup_field = 'pk'
    queryset = Student.objects.all()
