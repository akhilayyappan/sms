from django.urls import path
from .views import SchoolRegisterAPIView, SchoolLoginAPIView, SchoolUpdateRetrieveDeleteAPIView

urlpatterns = [
    path('register/', SchoolRegisterAPIView.as_view(), name='register'),
    path('login/', SchoolLoginAPIView.as_view(), name='login'),
    path('retrieve-update-delete/<int:pk>/', SchoolUpdateRetrieveDeleteAPIView.as_view(), name='retrieve-update-delete')
]
