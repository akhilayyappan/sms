from rest_framework.response import Response
from rest_framework.status import HTTP_200_OK, HTTP_400_BAD_REQUEST

from rest_framework.generics import (
    CreateAPIView,
    ListAPIView,
    UpdateAPIView,
    RetrieveAPIView,
    DestroyAPIView,
    RetrieveUpdateDestroyAPIView
)

from ..models import School

from rest_framework.permissions import (
    AllowAny,
    IsAuthenticated,
    IsAdminUser,
    IsAuthenticatedOrReadOnly,
)

from .serializers import SchoolCreateSerializer, SchoolLoginSerializer, SchoolDetailSerializer


class SchoolRegisterAPIView(CreateAPIView):
    """
    API to register schools
    """
    serializer_class = SchoolCreateSerializer
    queryset = School.objects.all()
    permission_classes = [AllowAny]


class SchoolLoginAPIView(CreateAPIView):
    """
    Schools login APIView
    """
    permission_classes = [AllowAny]
    serializer_class = SchoolLoginSerializer

    def post(self, request, *args, **kwargs):
        data = request.data
        serializer = SchoolLoginSerializer(data=data)
        if serializer.is_valid(raise_exception=True):
            new_data = serializer.data
            return Response(new_data, status=HTTP_200_OK)
        return Response(serializer.errors, status=HTTP_400_BAD_REQUEST)


class SchoolUpdateRetrieveDeleteAPIView(RetrieveUpdateDestroyAPIView):
    """
    API to Update/Detail/Delete of schools
    """
    serializer_class = SchoolDetailSerializer
    queryset = School.objects.all()
    lookup_field = 'pk'
    permission_classes = [IsAuthenticated]
