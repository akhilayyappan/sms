from rest_framework.serializers import (
    CharField,
    EmailField,
    ModelSerializer,
    ValidationError,
)
from ..models import School
from .utils import get_tokens_for_user


class SchoolCreateSerializer(ModelSerializer):
    password = CharField(write_only=True)
    confirm_password = CharField(write_only=True)

    class Meta:
        model = School
        fields = [
            'email',
            'name',
            'address',
            'state',
            'city',
            'country',
            'zip',
            'phone_number',
            'password',
            'confirm_password'
        ]

    def validate_email(self, value):
        email = value
        user_qs = School.objects.filter(email=email)
        if user_qs.exists():
            raise ValidationError("This school has already registered.")
        return value

    def validate_phone_number(self, value):
        phone_number = value
        user_qs = School.objects.filter(phone_number=phone_number)
        if user_qs.exists():
            raise ValidationError("This phone has already registered.")
        return value

    def validate_password(self, value):
        data = self.get_initial()
        password1 = value
        password2 = data.get('confirm_password')
        if password1 != password2:
            raise ValidationError('Passwords Must Match')
        return value

    def create(self, validated_data):
        name = validated_data.get('name', None)
        email = validated_data.get('email', None)
        address = validated_data.get('address', None)
        state = validated_data.get('state', None)
        city = validated_data.get('city', None)
        country = validated_data.get('country', None)
        zip = validated_data.get('zip', None)
        phone_number = validated_data.get('phone_number', None)
        user_obj = School(
            name=name,
            email=email,
            address=address,
            state=state,
            city=city,
            country=country,
            zip=zip,
            phone_number=phone_number,
            is_active=True,
            is_verified=False,
        )
        user_obj.set_password(validated_data['password'])
        user_obj.save()
        return validated_data


class SchoolLoginSerializer(ModelSerializer):
    """
    User Login Serializer
    """
    token = CharField(allow_blank=True, read_only=True)
    refresh_token = CharField(allow_blank=True, read_only=True)
    email = EmailField(label='Email Address')

    class Meta:
        model = School
        fields = [
            'id',
            'email',
            'password',
            'token',
            'refresh_token',
        ]
        extra_kwargs = {"password": {"write_only": True}}

    def validate(self, data):
        user_obj = None
        email = data.get("email", None)
        password = data["password"]
        if not email or not password:
            raise ValidationError("Email and password required")
        user = School.objects.filter(email=email).distinct()
        if user.exists() and user.count() == 1:
            user_obj = user.first()
        else:
            raise ValidationError('There is no user registered with %s' % email)
        if user_obj:
            if user_obj.password == '':
                raise ValidationError('You have not verified your account. Please verify it before login')
            if not user_obj.check_password(password):
                raise ValidationError('Incorrect credentials. Please try again')

        response_token = get_tokens_for_user(user_obj)
        data['token'] = response_token.get('access', None)
        data['refresh_token'] = response_token.get('refresh', None)
        data['id'] = user_obj.id
        return data


class SchoolDetailSerializer(ModelSerializer):
    class Meta:
        model = School
        fields = '__all__'
